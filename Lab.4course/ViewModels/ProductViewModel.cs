﻿using Lab._4course.Models;

namespace Lab._4course.ViewModels
{
    public class ProductViewModel : Product
    {
        public bool IsSelected { get; set; }
        public int Count { get; set; }
    }
}
