﻿using Lab._4course.Models;
using System.Collections.Generic;

namespace Lab._4course.ViewModels
{
    public class CreateOrderViewModel : Order
    {
        public List<ProductViewModel> Products { get; set; }
    }
}
