﻿using Lab._4course.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Lab._4course.Infra
{
    public class OrderDBContext : DbContext
    {
        public OrderDBContext()
        {
        }

        public OrderDBContext(DbContextOptions<OrderDBContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
      
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductsInOrder> ProductsInOrders { get; set; }
        public DbSet<Order> Orders { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductsInOrder>(entity =>
            {
                entity.HasKey(orderProd => new { orderProd.OrderId, orderProd.ProductId });

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.ProductsInOrders)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductsInOrders)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            var m = new Product() { Id = 1, Cost = 10, Name = "Milk", Description = "test descr" };
            var m1 = new Product() { Id = 2, Cost = 11, Name = "Milk1", Description = "test descr" };
            var m2 = new Product() { Id = 3, Cost = 12, Name = "Milk2", Description = "test descr" };
            var m3 = new Product() { Id = 4, Cost = 13, Name = "Milk3", Description = "test descr" };
            var m4 = new Product() { Id = 5, Cost = 14, Name = "Milk4", Description = "test descr" };
            var m5 = new Product() { Id = 6, Cost = 15, Name = "Milk5", Description = "test descr" };

            var o = new Order() { Id = 1, ReceiptDate = DateTime.UtcNow };
            var o1 = new Order() { Id = 2, ReceiptDate = DateTime.UtcNow };
            var o2 = new Order() { Id = 3, ReceiptDate = DateTime.UtcNow };
            var o3 = new Order() { Id = 4, ReceiptDate = DateTime.UtcNow };
            var o4 = new Order() { Id = 5, ReceiptDate = DateTime.UtcNow };
            var o5 = new Order() { Id = 6, ReceiptDate = DateTime.UtcNow };


            modelBuilder.Entity<Product>().HasData(m, m1, m2, m3, m4, m5);

            modelBuilder.Entity<Order>().HasData(o, o1, o2, o3, o4, o5);

            modelBuilder.Entity<ProductsInOrder>().HasData(
                new ProductsInOrder() { Count = 5, OrderId = o.Id, ProductId = m.Id },
                new ProductsInOrder() { Count = 15, OrderId = o1.Id, ProductId = m1.Id },
                new ProductsInOrder() { Count = 25, OrderId = o2.Id, ProductId = m2.Id }
                );
        }
    }
}
