﻿using Lab._4course.Infra;
using Lab._4course.Models;
using Lab._4course.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab._4course.Controllers
{
    public class OrdersController : Controller
    {
        private readonly OrderDBContext _context;

        public OrdersController(OrderDBContext context)
        {
            _context = context;
           
        }
        // GET: Orders
        public async Task<IActionResult> Index()
        {
            return View(await _context.Orders.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }
            
            var productIds = _context.ProductsInOrders.Where(p => p.OrderId == order.Id).ToList();

            var products = _context.Products.Where(p => productIds.Select(p => p.ProductId).Contains(p.Id)).ToList().Select( p => new ProductViewModel()
            {
                Description = p.Description,
                Cost = p.Cost,
                Count = productIds.Where(po => po.ProductId == p.Id).FirstOrDefault().Count,
                Name = p.Name,
                
            }).ToList();

            var orderViewModel = new CreateOrderViewModel()
            {
                Id = order.Id,
                ReceiptDate = order.ReceiptDate,
                Products = products
            };

            return View(orderViewModel);
        }

        // GET: Orders/Create
        public IActionResult Create()
        {
            ViewBag.Products = _context.Products.ToList();

            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateOrderViewModel orderViewModel)
        {
            if (ModelState.IsValid)
            {
                var orderTOAdd = new Order()
                {
                       ReceiptDate = orderViewModel.ReceiptDate
                };

                _context.Add(orderTOAdd);

                await _context.SaveChangesAsync();

                var productsToAdd = orderViewModel.Products.Where(p => p.IsSelected).ToList();

                _context.ProductsInOrders.AddRange(productsToAdd.Select(p => new ProductsInOrder()
                {
                    OrderId = orderTOAdd.Id,
                    ProductId = p.Id,
                    Count = p.Count
                }).ToList());

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(orderViewModel);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ReceiptDate")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [ActionName("GenerateOrder")]
        public async Task<IActionResult> GenerateOrder()
        {
          //  var orders = await GenerateOrderAsync();//await ContainProductAsync(_context.Products.FirstOrDefault());// await FilteredOrdersAsync(1000000, 1);

            // _context.Orders.Add(order);

            //// _context.ProductsInOrders.AddRange(order.ProductsInOrders);
            // await _context.SaveChangesAsync();
             return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }

        [HttpGet]
        [ActionName("FilteredOrdersAsync")]
        public async Task<IActionResult> FilteredOrdersAsync()
        {
            var r = await FilteredOrdersAsync1(100, 2);
            return View(r);
        }

        [HttpGet]
        [ActionName("ContainProductAsync")]
        public async Task<IActionResult> ContainProductAsync()
        {
            return View(await ContainProductAsync1(_context.Products.FirstOrDefault()));
        }

        [HttpGet]
        [ActionName("NotContaintProductsNotTodayAsync")]
        public async Task<IActionResult> NotContaintProductsNotTodayAsync()
        {
            return View(await NotContaintProductsNotTodayAsync1(_context.Products.FirstOrDefault()));
        }

        private Task<List<Order>> FilteredOrdersAsync1(int maxSum, int productsCount) => _context.Orders
                .Where(t => t.ProductsInOrders.Select(p=> p.ProductId).Distinct().Count() == productsCount)
                .Where(t => t.ProductsInOrders.Sum(t => t.Product.Cost) <= maxSum).ToListAsync();

        private Task<List<int>> ContainProductAsync1(Product product) => _context.Orders
            .Where(t => t.ProductsInOrders.Any(p => p.ProductId == product.Id))
            .Select(t => t.Id).ToListAsync();

        private Task<List<int>> NotContaintProductsNotTodayAsync1(Product product) => _context.Orders
            .Where(t => !t.ProductsInOrders.Any(p => p.ProductId == product.Id))
            .Where(t => t.ReceiptDate.Day == DateTime.Now.Day)
            .Select(t => t.Id).ToListAsync();

        [HttpGet]
        [ActionName("RemoveAsync")]
        public async Task<IActionResult> RemoveAsync()
        {
            await RemoveAsync1(_context.Products.FirstOrDefault(), 5);

            return RedirectToAction(nameof(Index));
        }

        public async Task RemoveAsync1(Product product, int count)
        {
            _context.Orders.RemoveRange(
                _context.Orders
                .Where(t => t.ProductsInOrders.Any(p => p.ProductId == product.Id))
                .Where(t => t.ProductsInOrders.Any(p => p.Count == count)).AsEnumerable());

            await _context.SaveChangesAsync();

        }

        //private async Task<Order> GenerateOrderAsync()
        //{
        //    //var products = await _context.Orders
        //    //    .Where(t => t.ReceiptDate.Day == DateTime.Now.Day)
        //    //    .SelectMany(t => t.ProductsInOrders)
        //    //    .Distinct().ToListAsync();

        //    var products = await _context.ProductsInOrders.Where(t => t.Order.ReceiptDate.Day == DateTime.Now.Day).ToListAsync();

        //    var order = new Order()
        //    {
        //        // Id = 7,
        //        ReceiptDate = DateTime.Now,
        //        // ProductsInOrders = products
        //    };
        //    using var transasction = await _context.Database.BeginTransactionAsync();
        //    await _context.Orders.AddAsync(order);

        //    await _context.SaveChangesAsync();

        //    var newProds = products.Select(t=> new ProductsInOrder() { Count = t.Count, ProductId = t.ProductId}).ToList();
        //    newProds.ForEach(t => t.OrderId = order.Id);
        //    await _context.ProductsInOrders.AddRangeAsync(products);
        //    await _context.SaveChangesAsync();

        //    await transasction.CommitAsync();
        //    return order;
        //}
    }
}
