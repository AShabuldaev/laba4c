﻿using System;
using System.Collections.Generic;

namespace Lab._4course.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime ReceiptDate { get; set; }

        public virtual List<ProductsInOrder> ProductsInOrders { get; set; }
    }
}
