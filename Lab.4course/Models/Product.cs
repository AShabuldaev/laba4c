﻿using System.Collections.Generic;

namespace Lab._4course.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public virtual List<ProductsInOrder> ProductsInOrders { get; set; }
    }
}
