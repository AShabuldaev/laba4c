﻿namespace Lab._4course.Models
{
    public class ProductsInOrder
    {
        public virtual Product Product { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }

        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }
}
